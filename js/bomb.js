// @format
'use strict';

window.bombs = [];

function createBomb(x, y) {
  bombs[bombs.length] = {
    x: x,
    y: y,
    radius: 0.01,
    //radius: 0.5,
    lifetime: 0,
    DAMAGE: 5,
    SPEED: 5,
    MAX_LIFETIME: 0.4,
    RADIUS_INCREASING: 2,
  };

  screenshake(0.16, 0.4);
  //AUDIO.boom.play();
}

function updateBombs() {
  if (!bombs.length) return;

  for (let i = bombs.length; i--; ) {
    let bomb = bombs[i];

    if (bomb.lifetime > bomb.MAX_LIFETIME) {
      remove_at_fast(bombs, i);
      continue;
    }

    bomb.radius +=
      bomb.RADIUS_INCREASING *
      sign(bomb.MAX_LIFETIME / 2 - bomb.lifetime) *
      GAME.deltaTime;
    bomb.lifetime += GAME.deltaTime;

    for (const mush of window.Mushrooms) {
      if (
        does_circles_collides(
          bomb.x,
          bomb.y,
          bomb.radius * 2,
          mush.x,
          mush.y,
          mush.radius * 2,
        )
      ) {
        HitMushroom(mush, bomb);
        //AUDIO.damageMush.play();
      }
    }
  }
}

function drawBombs() {
  if (!bombs.length) return;
  noStroke();
  let cellSize = gameMap.TILE_SIZE * GAME.screenSize;
  bombs.forEach(bomb => {
    for (let i = 1; i < gameColor.tab.length; i++) {
      if (floor(bomb.radius * 100) % 2) fill(gameColor.tab[i]);
      else fill(gameColor.tabInverse[i]);
      ellipse(
        (bomb.x - player.x) * cellSize,
        (bomb.y - player.y) * cellSize,
        (bomb.radius / i) * GAME.screenSize,
        (bomb.radius / i) * GAME.screenSize,
      );
    }
  });
}
