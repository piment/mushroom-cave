fakePlayer = {
  fireTimer: 0,
  fireRate: 1.5,
  fireDuration: 0.75,

  turnDecisionTimer: 0,
  turnDecisionRateMin: 1,
  turnDecisionRateMax: 0.75,
  turnRateDurationMin: 0.5,
  turnRateDurationMax: 1.5,

  isFiring: false,
  isTurning: 0,

  isKillingEveryone: false,
}

fakeBullet = {
  DAMAGE: 0.005,
}

function updateMenu()
{
  if(keyIsDown(32))
  {
    GAME.isInMenu = false;
    lvlUp();
  }

  updateFakePlayer();

  if(window.Mushrooms.length >= 350)
  {
    fakePlayer.isKillingEveryone = true;
  }

  if(window.Mushrooms.length <= 20)
  {
    fakePlayer.isKillingEveryone = false;
  }

  if(fakePlayer.isKillingEveryone)
  {
    for(const mush of Mushrooms)
    {
      HitMushroom(mush, fakeBullet);
    }
  }
}

function updateFakePlayer()
{
  fakePlayer.fireTimer += fakePlayer.isFiring ? -GAME.deltaTime : fakePlayer.fireRate * GAME.deltaTime;
  fakePlayer.turnDecisionTimer += fakePlayer.isTurning !== 0 ? -GAME.deltaTime : random(fakePlayer.turnDecisionRateMin, fakePlayer.turnDecisionRateMin) * GAME.deltaTime;

  if(fakePlayer.fireTimer > 1)
  {
    fakePlayer.isFiring = true;
    fakePlayer.fireTimer = fakePlayer.fireDuration;
  }
  else if(fakePlayer.fireTimer < 0)
  {
    fakePlayer.isFiring = false;
    fakePlayer.fireTimer = 0;
  }

  if(fakePlayer.turnDecisionTimer > 1)
  {
    fakePlayer.isTurning = random() < 0.5 ? -1 : 1;
    fakePlayer.turnDecisionTimer = random(fakePlayer.turnRateDurationMin, fakePlayer.turnRateDurationMax);
  }
  else if(fakePlayer.turnDecisionTimer < 0)
  {
    fakePlayer.isTurning = 0;
    fakePlayer.turnDecisionTimer = 0;
  }

  player.speed = player.ACCELERATION * GAME.deltaTime;

  player.dir += fakePlayer.isTurning * player.ROTATION_SPEED * GAME.deltaTime;

  let nextPos = createVector(
    player.x + cos(player.dir) * player.speed,
    player.y - sin(player.dir) * player.speed,
  );
  
  if (player.checkCollisions) {
    nextPos = handlePlayerMushCollisions(nextPos);
    nextPos = handlePlayerWallCollisions(nextPos);
  }

  player.x = nextPos.x;
  player.y = nextPos.y;

  if (fakePlayer.isFiring && player.canFire) {
    if (GAME.t > player.lastFireTime) {
      player.lastFireTime = GAME.t + player.FIRE_RATE;
      createBullet(nextPos.x, nextPos.y, player.dir +
        random(
          -player.BULLET_SPREADING_FACTOR,
          player.BULLET_SPREADING_FACTOR,
        ));
    }
    player.fireDuration += GAME.deltaTime;
    if (player.fireDuration >= player.FIRE_MAX_DURATION) player.canFire = false;
  } else {
    player.fireDuration -= GAME.deltaTime;

    if (player.fireDuration < 0) {
      player.fireDuration = 0;
      player.canFire = true;
    }
  }

  player.oldDir += (player.dir - player.oldDir) * 16 * GAME.deltaTime;
}