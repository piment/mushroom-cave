'use strict';

function randomRange(min, max) {
  return Math.random() * (max - min) + min;
}

window.MushroomsTypes = {
  BASIC: 0,
  FAST: 4,
  MOVER: 6
}

window.MushRoomDifficultyScaling = {
  spawningProbability: 0,
  possibleMushroomTypes: [ window.MushroomsTypes.BASIC ],

  spawningProbabilityMultiplier: 1.1,
  canSpawnFastMushroomFromLevel: 2,
  canSpawnMoverMushroomFromLevel: 4,
}

window.BasicMushroom = function(x, y, growthDir) {
  this.x = x * gameMap.w; 
  this.y = y * gameMap.h;
  this.radius = 0.1;
  this.displayRadius = this.radius;
  this.boomTimer = 0;
  this.boomPerSecond = 12;
  this.isBoomed = false;
  this.maxRadius = 0.3;
  this.deathRadius = 0.05;
  this.spawnTimer = 0.5; 
  this.growthBuffer = 0;
  this.growthThershold = 0.025; 
  this.growthRate = randomRange(0.01, 0.025);
  this.spawnRate = 2;
  this.childType = window.MushroomsTypes.BASIC;
  this.growthDir = !!growthDir ? growthDir : randomRange(0, TWO_PI);
  
}

window.FastMushroom = function(x, y, growthDir) {
  this.x = x * gameMap.w; 
  this.y = y * gameMap.h;
  this.radius = 0.05;
  this.displayRadius = this.radius;
  this.boomTimer = 0;
  this.boomPerSecond = 15;
  this.isBoomed = false;
  this.maxRadius = 0.15;
  this.deathRadius = 0.04;
  this.spawnTimer = 0.8; 
  this.color = '#db7';
  this.growthBuffer = 0;
  this.growthThershold = 0.02; 
  this.growthRate = randomRange(0.025, 0.05);
  this.spawnRate = 4;
  this.childType = window.MushroomsTypes.FAST;
  this.growthDir = !!growthDir ? growthDir : randomRange(0, TWO_PI);
}

window.MoverMushroom = function(x, y, growthDir) {
  this.x = x * gameMap.w; 
  this.y = y * gameMap.h;
  this.radius = 0.05;
  this.displayRadius = this.radius;
  this.boomTimer = 0;
  this.boomPerSecond = 15;
  this.isBoomed = false;
  this.maxRadius = 0.15;
  this.deathRadius = 0.04;
  this.spawnTimer = 0.5; 
  this.color = '#db7';
  this.growthBuffer = 0;
  this.growthThershold = 0.02; 
  this.growthRate = randomRange(0.01, 0.025);
  this.spawnRate = 2;
  this.childType = window.MushroomsTypes.MOVER;
  this.growthDir = !!growthDir ? growthDir : randomRange(0, TWO_PI);

  this.moveSpeed = 0.5;
  this.moveSpeedDegrade = randomRange(0.25, 0.5);
}

function initMushrooms(level) 
{
  window.Mushrooms = [];
  MushRoomDifficultyScaling.possibleMushroomTypes = [];

  MushRoomDifficultyScaling.possibleMushroomTypes.push(MushroomsTypes.BASIC)
  
  if(level >= MushRoomDifficultyScaling.canSpawnFastMushroomFromLevel)
  {
    MushRoomDifficultyScaling.possibleMushroomTypes.push(MushroomsTypes.FAST);
  }
  if(level >= MushRoomDifficultyScaling.canSpawnMoverMushroomFromLevel)
  {
    MushRoomDifficultyScaling.possibleMushroomTypes.push(MushroomsTypes.MOVER);
  }
  if(level == 0)
  {
    MushRoomDifficultyScaling.spawningProbability = 0.1;
  }
  else
  {
    MushRoomDifficultyScaling.spawningProbability *= MushRoomDifficultyScaling.spawningProbabilityMultiplier;
  }

  initMushroomMap();
  initInitialMushrooms();
}

function initMushroomMap()
{
  window.mushroomMap = [];

  for(let i = 0; i < gameMap.h*4; i++)
  {
    mushroomMap.push([]);
    for(let j = 0; j < gameMap.w*4; j++)
    {
      mushroomMap[i].push(0);
    }
  }
}

function initInitialMushrooms()
{
  if(GAME.isInMenu)
  {
    MushRoomDifficultyScaling.spawningProbability *= 2;
  }
  
  for(let i = 0; i < gameMap.h; i++)
  {
    for(let j = 0; j < gameMap.w; j++)
    {
      let rnd = Math.random();
      let stuff = random(MushRoomDifficultyScaling.possibleMushroomTypes);

      if((i == floor(player.y) || i == floor(player.y+1) || i == floor(player.y-1)) && (j == floor(player.x) || j == floor(player.x+1) || j == floor(player.x-1)))
      {
        continue;
      }

      if(gameMap.tiles[i][j] == "." && rnd <= MushRoomDifficultyScaling.spawningProbability)
      {
        let mush =  new MushroomFactory(stuff, (j+0.25) / gameMap.w, (i+0.25) / gameMap.h);
        //mush.radius = mush.maxRadius;
        //mush.displayRadius = mush.maxRadius;
        Mushrooms[Mushrooms.length] = mush;
      }
    }
  }

  if(GAME.isInMenu)
  {
    MushRoomDifficultyScaling.spawningProbability /= 2;
  }
}

function MushroomFactory(type, x, y, generation, growthDir)
{
  switch(type)
  {
    case window.MushroomsTypes.BASIC: return new window.BasicMushroom(x, y, generation, growthDir);
    case window.MushroomsTypes.FAST: return new window.FastMushroom(x, y, generation, growthDir);
    case window.MushroomsTypes.MOVER: return new window.MoverMushroom(x, y, generation, growthDir);
    default: console.error("MushroomFactory >> Invalid mushroom type passed"); break;
  }
}

window.Mushrooms = [];

function drawMushrooms()
{
  window.DebugHTML.innerText = "Mushrooms: " + window.Mushrooms.length + "   Framerate: " + (frameRate() | 0);
  window.ScoreHTML.innerText = "Score: " + GAME.score;
  window.LevelHTML.innerText = "Level: " + GAME.lvl;

  let cellSize = gameMap.TILE_SIZE * GAME.screenSize;
  let drawDist = player.VIEW_DIST * cellSize / 2 * 1.5;

  //stroke("black")
  for (const mush of window.Mushrooms) 
  {
    
		let posX = (mush.x - player.x) * cellSize;
		let posY = (mush.y - player.y) * cellSize;
    
    if(abs(posX) > drawDist || abs(posY) > drawDist)
    {
      continue;
    }
    
    fill(gameColor.medium);

    if(mush.childType == MushroomsTypes.BASIC)
    {
      ellipse(posX, posY, mush.displayRadius * GAME.screenSize);
    }
    else
    {
      drawPolygon(posX, posY, mush.displayRadius / 2 * GAME.screenSize, mush.childType)
    }
  }

  //noStroke();
}

function drawPolygon(x, y, radius, npoints) {
  var angle = TWO_PI / npoints;
  beginShape();
  for (var a = 0; a < TWO_PI; a += angle) {
    var sx = x + cos(a) * radius;
    var sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

function updateMushrooms()
{
  for (let i = window.Mushrooms.length-1; i >= 0; i--) 
  {
    let mush = window.Mushrooms[i];

    if(mush.radius < mush.maxRadius)
    {
      GrowMushroom(mush);
      boomMushroom(mush);
    }
    else
    {
      MakeBabies(mush)
    }
    
    if(mush.radius < mush.deathRadius)
    {
      killMushroom(i);
    }

    if(mush.childType == MushroomsTypes.MOVER)
    {
      moveMushroom(mush);
    }
  }
}

function moveMushroom(mush)
{
  if(mush.moveSpeed <= 0)
  {
    return;
  }

  let nextX = mush.x + Math.cos(mush.growthDir) * mush.moveSpeed * GAME.deltaTime;
  let nextY = mush.y - Math.sin(mush.growthDir) * mush.moveSpeed * GAME.deltaTime;

  let cellSize = gameMap.TILE_SIZE * GAME.screenSize;
  let realX = nextX * cellSize;
  let realY = nextY * cellSize;

  let tX = Math.floor(nextX);
  let tY = Math.floor(nextY);

  if(!gameMap.tiles[tY] || !gameMap.tiles[tY][tX] || gameMap.tiles[tY][tX] != ".")
  {
    mush.moveSpeed = 0;
    return;
  }

  mush.x = nextX;
  mush.y = nextY;
  mush.moveSpeed -= mush.moveSpeedDegrade * GAME.deltaTime;
}

function boomMushroom(mush)
{
  mush.displayRadius = mush.isBoomed ? mush.radius + mush.growthThershold : mush.radius;

  if(mush.growthBuffer < 0.7 * mush.growthThershold)
  {
    return;
  }

  mush.boomTimer += mush.boomPerSecond * GAME.deltaTime;

  if(mush.boomTimer >= 1)
  {
    mush.isBoomed = !mush.isBoomed;
    mush.boomTimer = 0;
  }
}

function MakeBabies(mush)
{
  mush.spawnTimer += mush.spawnRate * GAME.deltaTime;

  if(mush.spawnTimer >= 1)
  {
    SpawnMushroomChild(mush);
    mush.spawnTimer = 0;
  }
}

function SpawnMushroomChild(mush)
{
  let rndAngle =  randomRange(0, TWO_PI)//randomRange(mush.growthDir - PI/4, mush.growthDir + PI/4);

  let cellSize = gameMap.TILE_SIZE * GAME.screenSize;
  let realX = mush.x * cellSize;
  let realY = mush.y * cellSize;
  let realRadius = mush.radius * GAME.screenSize / 2;

  let newRealX = realX + Math.cos(rndAngle) * realRadius;
  let newRealY = realY - Math.sin(rndAngle) * realRadius;

  let newX = newRealX / cellSize / gameMap.w;
  let newY = newRealY / cellSize / gameMap.h;

  let tX = Math.floor(newX * gameMap.w);
  let tY = Math.floor(newY * gameMap.h);

  let mmX = Math.floor(newRealX / (cellSize/4));
  let mmY = Math.floor(newRealY / (cellSize/4));

  if(!mushroomMap[mmY] || mmX > gameMap.w*4 || mmX < 0 || mushroomMap[mmY][mmX] >= 1)
  {
    return;
  }

  if(!gameMap.tiles[tY] || !gameMap.tiles[tY][tX] || gameMap.tiles[tY][tX] != ".")
  {
    return;
  }

  mushroomMap[mmY][mmX]++;

  let mushroom = MushroomFactory(mush.childType, newX, newY, rndAngle);
  window.Mushrooms.push(mushroom);
}

function GrowMushroom(mush)
{
  mush.growthBuffer += mush.growthRate * GAME.deltaTime;

  if(mush.growthBuffer >= mush.growthThershold)
  {
    mush.radius += mush.growthBuffer;
    mush.growthBuffer = 0;
  }
}

function HitMushroom(mush, damager)
{
  mush.radius = max(0, mush.radius - damager.DAMAGE);
}

function killMushroom(i)
{
  let mush = window.Mushrooms[i];

  let cellSize = gameMap.TILE_SIZE * GAME.screenSize;
  let realX = mush.x * cellSize;
  let realY = mush.y * cellSize;

  let mmX = Math.floor(realX / (cellSize/4));
  let mmY = Math.floor(realY / (cellSize/4));

  mushroomMap[mmY][mmX]--;
  remove_at_fast(window.Mushrooms, i);

  GAME.score += 10;
}