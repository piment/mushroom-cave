// @format
'use strict';

window.bullets = [];

function createBullet(x, y, dir) {
  bullets[bullets.length] = {
    x: x,
    y: y,
    dir: dir,
    radius: 0.01,
    DAMAGE: 0.07,
    SPEED: 4,
    MAX_RADIUS_INCREASING_FACTOR: 0.02,
  };
  //AUDIO.shoot.play();
}

function updateBullets() {
  if (!bullets.length) return;

  for (let i = bullets.length; i--; ) {
    let bullet = bullets[i];

    bullet.x += cos(bullet.dir) * bullet.SPEED * GAME.deltaTime;
    bullet.y -= sin(bullet.dir) * bullet.SPEED * GAME.deltaTime;
    bullet.radius +=
      random(bullet.MAX_RADIUS_INCREASING_FACTOR) * GAME.deltaTime;

    if (
      !gameMap.tiles[floor(bullet.y)] ||
      !/[.E]/.test(gameMap.tiles[floor(bullet.y)][floor(bullet.x)])
    ) {
      remove_at_fast(bullets, i);
      continue;
    }

    let destroyed = false;
    for (const mush of window.Mushrooms) {
      if (
        does_circles_collides(
          bullet.x,
          bullet.y,
          bullet.radius * 2,
          mush.x,
          mush.y,
          mush.radius * 2,
        )
      ) {
        HitMushroom(mush, bullet);
        //AUDIO.damageMush.play();
        destroyed = true;
      }
    }
    if (destroyed) {
      screenshake(0.04, 0.07);
      remove_at_fast(bullets, i);
    }
  }
}

function drawBullets() {
  noStroke();
  fill(gameColor.bright);
  let cellSize = gameMap.TILE_SIZE * GAME.screenSize;
  bullets.forEach(bullet => {
    ellipse(
      (bullet.x - player.x) * cellSize,
      (bullet.y - player.y) * cellSize,
      bullet.radius * GAME.screenSize,
      bullet.radius * GAME.screenSize,
    );
  });
}
