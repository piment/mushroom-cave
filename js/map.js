// @format
'use strict';

window.gameMap = {
  tiles: null,
  w: 0,
  h: 0,
  cellsNb: 0,
  wallsNb: 0,
  TILE_SIZE: 0.5,
  BASE_W: 4,
  BASE_H: 4,
};

function setupMap() {
  gameMap.tiles = createNewMap();
  gameMap.h = gameMap.tiles.length;
  gameMap.w = gameMap.tiles[0].length;
  gameMap.cellsNb = gameMap.w * gameMap.h;
  gameMap.wallsNb = gameMap.tiles.reduce(
    (acc, row) => acc + (row.match(/X/g) || []).length,
    0,
  );
}

function createNewMap() {
  const rndSize = s => floor(s * log(3 + random(GAME.lvl)));
  const w = rndSize(gameMap.BASE_W);
  const h = rndSize(gameMap.BASE_H);

  const wallsNb = w * (h - 1) + (w - 1) * h;
  const wallIsCol = i => i % (w * 2 - 1) < w - 1;
  const wallX = i => (i % (w * 2 - 1)) - (wallIsCol(i) ? 0 : w - 1);
  const wallY = i => Math.floor(i / (w * 2 - 1));
  const cellI = (x, y) => y * w + x;
  const wallI = (x, y) => (x >> 1) + (y >> 1) + y * (w - 1);

  let cells = Array(w * h)
    .fill(0)
    .map((_, i) => i);

  let walls = Array(wallsNb).fill(true);

  let unvisitedWalls = Array(wallsNb)
    .fill(0)
    .map((_, i) => i);

  while (unvisitedWalls.length) {
    let i = floor(random(unvisitedWalls.length));
    let wall = unvisitedWalls[i];
    let a, b;

    if (wallIsCol(wall)) {
      a = cellI(wallX(wall), wallY(wall));
      b = cellI(wallX(wall) + 1, wallY(wall));
    } else {
      a = cellI(wallX(wall), wallY(wall));
      b = cellI(wallX(wall), wallY(wall) + 1);
    }

    if (cells[a] !== cells[b]) {
      let v = min(cells[a], cells[b]);
      cells = cells.map(c => (c == cells[a] || c == cells[b] ? v : c));
      walls[wall] = false;
    }

    remove_at_fast(unvisitedWalls, i);
  }

  let newMap = [];
  for (let y = 0; y < h * 2 - 1; ++y) {
    newMap[y] = ``;
    for (let x = 0; x < w * 2 - 1; ++x) {
      let c = `.`;
      if (y & 1) {
        if (x & 1 || walls[wallI(x, y)]) c = `X`;
      } else {
        if (x & 1 && walls[wallI(x, y)]) c = `X`;
      }
      newMap[y] += c;
    }
  }
  newMap[0] = newMap[0].slice(0, -1) + `E`;

  return newMap;
}

function drawMap() {
  noStroke();

  for (
    let y = floor(player.y - player.VIEW_DIST);
    floor(y < player.y + player.VIEW_DIST);
    ++y
  ) {
    if (y < 0 || y >= gameMap.h) continue;

    for (
      let x = floor(player.x - player.VIEW_DIST);
      floor(x < player.x + player.VIEW_DIST);
      ++x
    ) {
      if (x < 0 || x >= gameMap.tiles[y].length) continue;

      let size = gameMap.TILE_SIZE * GAME.screenSize;
      let posX = (x - player.x) * size;
      let posY = (y - player.y) * size;

      switch (gameMap.tiles[y][x]) {
        case 'X':
          fill(gameColor.bright);
          rect(posX, posY, size, size);
          break;
        case 'E':
          fill(0);
          rect(posX, posY, size, size);
          break;
        case '.':
          let s = size / 2;
          fill(gameColor.groundA);
          rect(posX, posY, s, s);
          rect(posX + s, posY + s, s, s);
          fill(gameColor.groundB);
          rect(posX + s, posY, s, s);
          rect(posX, posY + s, s, s);
          break;
      }
    }
  }
}
