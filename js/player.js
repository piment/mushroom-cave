// @format
'use strict';

window.player = {
  bombsNb: 3,
  DAMAGE: 0.12, // TODO: to remove?
  ROTATION_SPEED: Math.PI * 1.0,
  ACCELERATION: 2,
  SCREEN_X: 0,
  SCREEN_Y: 0.25,
  VIEW_DIST: 4,
  RADIUS: 0.02,
  FIRE_RATE: 0.045, // seconds
  BOMB_RATE: 1.0,
  FIRE_MAX_DURATION: 2.5,
  BULLET_SPREADING_FACTOR: Math.PI * 0.03,
};

function resetPlayer() {
  setupPlayer();
  player.bombsNb = 3;
}

function setupPlayer() {
  player.x = 0.5;
  player.y = gameMap.h - 0.5;
  player.dir = HALF_PI;
  player.oldDir = HALF_PI;
  player.speed = 0;
  player.lastFireTime = 0;
  player.lastBombTime = 0;
  player.fireDuration = 0;
  player.canFire = true;
  player.checkCollisions = true;
  player.bombsNb = min(10, player.bombsNb + 1);
}

function updatePlayer() {
  if (PAUSE.isPaused) return;

  let nextPos = handlePlayerMoves();

  nextPos = handlePlayerBulletFire(nextPos);

  handlePlayerBombFire(nextPos);

  if (player.checkCollisions) {
    nextPos = handlePlayerMushCollisions(nextPos);
    nextPos = handlePlayerWallCollisions(nextPos);
  }

  if (
    gameMap.tiles[floor(nextPos.y)] &&
    /[.E]/.test(gameMap.tiles[floor(nextPos.y)][floor(nextPos.x)])
  ) {
    player.x = nextPos.x;
    player.y = nextPos.y;
  } else if (
    !gameMap.tiles[floor(player.y)] ||
    !/[.E]/.test(gameMap.tiles[floor(player.y)][floor(player.x)])
  ) {
    console.log('something goes wrong...');
    player.x = (gameMap.w >> 1) + 0.5;
    player.y = gameMap.h - 0.5;
  }

  player.oldDir += (player.dir - player.oldDir) * 16 * GAME.deltaTime;
}

function drawPlayer() {
  noStroke();
  fill(gameColor.bright);

  let angle = radians(120);
  let deltaDir = PI * 0.5 + (player.dir - player.oldDir);
  triangle(
    cos(deltaDir) * player.RADIUS * GAME.screenSize,
    (player.SCREEN_Y - sin(deltaDir) * player.RADIUS) * GAME.screenSize,
    cos(deltaDir + angle) * player.RADIUS * GAME.screenSize,
    (player.SCREEN_Y - sin(deltaDir + angle) * player.RADIUS) * GAME.screenSize,
    cos(deltaDir - angle) * player.RADIUS * GAME.screenSize,
    (player.SCREEN_Y - sin(deltaDir - angle) * player.RADIUS) * GAME.screenSize,
  );
}

function handlePlayerBombFire(nextPos) {
  if (keyIsDown(SHIFT) && player.bombsNb > 0) {
    if (GAME.t > player.lastBombTime) {
      player.lastBombTime = GAME.t + player.BOMB_RATE;

      for (let i = 0; i < 10; ++i) {
        window.setTimeout(() => {
          let dir = player.dir + random(-0.2, 0.2);
          let radius = 0.4 * (i + 1);
          createBomb(
            nextPos.x + cos(dir) * radius,
            nextPos.y - sin(dir) * radius,
          );
        }, i * 100);
      }
      player.bombsNb -= 1;
    }
  }
}

function handlePlayerBulletFire(nextPos) {
  if (keyIsDown(32) && player.canFire) {
    if (GAME.t > player.lastFireTime) {
      player.lastFireTime = GAME.t + player.FIRE_RATE;

      createBullet(
        nextPos.x,
        nextPos.y,
        player.dir +
          random(
            -player.BULLET_SPREADING_FACTOR,
            player.BULLET_SPREADING_FACTOR,
          ),
      );

      let kickSpeed = -player.ACCELERATION * 0.2 * GAME.deltaTime;
      return createVector(
        nextPos.x + cos(player.dir) * kickSpeed,
        nextPos.y - sin(player.dir) * kickSpeed,
      );
    }
    player.fireDuration += GAME.deltaTime;
    if (player.fireDuration >= player.FIRE_MAX_DURATION) player.canFire = false;
  } else {
    player.fireDuration -= GAME.deltaTime;

    if (player.fireDuration < 0) {
      player.fireDuration = 0;
      player.canFire = true;
    }
  }
  return nextPos;
}

function handlePlayerMoves() {
  if (keyIsDown(UP_ARROW)) player.speed = player.ACCELERATION * GAME.deltaTime;
  else if (keyIsDown(DOWN_ARROW))
    player.speed = -player.ACCELERATION * 0.95 * GAME.deltaTime;
  else player.speed = 0;

  if (keyIsDown(LEFT_ARROW))
    player.dir += player.ROTATION_SPEED * GAME.deltaTime;
  if (keyIsDown(RIGHT_ARROW))
    player.dir -= player.ROTATION_SPEED * GAME.deltaTime;

  return createVector(
    player.x + cos(player.dir) * player.speed,
    player.y - sin(player.dir) * player.speed,
  );
}

function handlePlayerMushCollisions(nextPos) {
  for (const mush of window.Mushrooms) {
    if (
      does_circles_collides(
        player.x,
        player.y,
        player.RADIUS,
        mush.x,
        mush.y,
        mush.radius,
      )
    ) {
      HitMushroom(mush, player);
      gameOver();

      let impactSpeed = player.ACCELERATION * GAME.deltaTime * 2.5;
      return createVector(
        player.x + sign(player.x - mush.x) * impactSpeed,
        player.y + sign(player.y - mush.y) * impactSpeed,
      );
    }
  }
  return nextPos;
}

function handlePlayerWallCollisions(nextPos) {
  let wallCollisionDist = player.RADIUS * sign(player.speed);

  let nextCellX = floor(nextPos.x + cos(player.dir) * wallCollisionDist);
  let nextCellY = floor(nextPos.y - sin(player.dir) * wallCollisionDist);

  if (gameMap.tiles[nextCellY]) {
    switch (gameMap.tiles[nextCellY][floor(player.x)]) {
      case '.':
        break;
      case 'E':
        lvlUp();
        break;
      case 'W':
      default:
        nextPos.y = player.y;
        break;
    }
  } else {
    nextPos.y = player.y;
  }

  if (gameMap.tiles[floor(player.y)]) {
    switch (gameMap.tiles[floor(player.y)][nextCellX]) {
      case '.':
        break;
      case 'E':
        lvlUp();
        break;
      case 'W':
      default:
        nextPos.x = player.x;
        break;
    }
  }

  return nextPos;
}
