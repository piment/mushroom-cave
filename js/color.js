// @format
'use strict';

window.gameColor = {};

function updateColors() {
  let hueVal = 180 + sin(GAME.t * 0.06) * 180;
  colorMode(HSB, 360, 100, 100, 1);

  gameColor.groundA = color(hueVal, 20, 100);
  gameColor.groundB = color(hueVal, 20, 92);
  gameColor.bomb = color(hueVal, 50, 50);
  gameColor.medium = color(hueVal, 100, 50);
  gameColor.bright = color(hueVal, 100, 100);
  gameColor.darkAlpha = color(hueVal, 80, 20, 0.65);
  gameColor.lightAlpha = color(hueVal, 80, 100, 0.85);
  gameColor.tab = [
    gameColor.ground,
    gameColor.bomb,
    gameColor.medium,
    gameColor.bright,
  ];
  gameColor.tabInverse = [
    gameColor.ground,
    gameColor.bright,
    gameColor.medium,
    gameColor.bomb,
  ];
}
