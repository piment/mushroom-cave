// @format
'use strict';

window.UI_DRAW_OPTIM = {
  hasdrawGameUI: false,
  lastBombsNb: 0,
};

window.gameUI = {
  fireBar: {
    x: -0.49,
    y: -0.45,
    w: 0.2,
    h: 0.02,
  },

  coolDownHeader: {
    x: 0.01,
    y: -0.03,
    textSize: 0.018,
  },

  bombHeader: {
    x: 0.01,
    y: 0.04,
    textSize: 0.018,
  },

  bombIcons: {
    x: 0.006,
    xInterval: 0.035,
    y: 0.12,
  },

  pauseHeader: {
    x: 0.5,
    y: 0.3,
    textSize: 0.09,
  },

  gameOverHeader: {
    x: 0.5,
    y: 0.3,
    textSize: 0.1,
  },

  gameOverPressPlay: {
    x: 0.5,
    y: 0.85,
    textSize: 0.035,
  },

  gameOverScore: {
    x: 0.5,
    y: 0.55,
    textSize: 0.028,
  },

  gameOverLevel: {
    x: 0.5,
    y: 0.65,
    textSize: 0.025,
  },

  gameGraphic: null,
  menuGraphic: null,

  menuTitle: {
    x: 0.5,
    y: 0.25,
    textSize: 0.09,
  },

  menuPressPlay: {
    x: 0.5,
    y: 0.6,
    textSize: 0.04,
  },

  menuMoveKeysHeader: {
    x: 0.0825,
    y: 0.79,
    textSize: 0.025,
  },

  menuArrowsIcon: {
    x: 0.025,
    y: 0.8,
  },

  menuShootKeysHeader: {
    x: 0.5125,
    y: 0.86,
    textSize: 0.025,
  },

  menuSpaceBarIcon: {
    x: 0.475,
    y: 0.84,
  },

  menuBombKeysHeader: {
    x: 0.65,
    y: 0.86,
    textSize: 0.025,
  },

  menuBombIcon: {
    x: 0.625,
    y: 0.87,
  },
};

function setupTextGraphics() {
  window.LoadingHTML.style.display = 'none';
  gameUI.coolDownHeader.textSize *= width;
  gameUI.pauseHeader.textSize *= width;
  gameUI.gameGraphic = createGraphics(width, height);
  gameUI.gameGraphic.textFont(FONTS.menu);
  gameUI.gameGraphic.textAlign(CENTER);

  gameUI.menuGraphic = createGraphics(width, height);
  gameUI.menuGraphic.textFont(FONTS.menu);
  gameUI.menuGraphic.textAlign(CENTER);
  gameUI.menuTitle.textSize *= width;
  gameUI.menuPressPlay.textSize *= width;
  gameUI.menuMoveKeysHeader.textSize *= width;
  gameUI.menuShootKeysHeader.textSize *= width;
  gameUI.gameOverHeader.textSize *= width;
  gameUI.gameOverPressPlay.textSize *= width;
  gameUI.menuBombKeysHeader.textSize *= width;
  gameUI.bombHeader.textSize *= width;
  gameUI.gameOverScore.textSize *= width;
  gameUI.gameOverLevel.textSize *= width;

  window.DebugHTML.style.top = 0.85 * height + 'px';
  window.DebugHTML.style.left = 0.9 * width + 'px';

  window.ScoreHTML.style.top = -0.03 * height + 'px';
  window.ScoreHTML.style.left = 0.9 * width + 'px';

  window.LevelHTML.style.top = 0.015 * height + 'px';
  window.LevelHTML.style.left = 0.9 * width + 'px';

  window.CooldownHeader.innerText = 'Weapon Cooldown';
  window.CooldownHeader.style.left = gameUI.coolDownHeader.x * width + 'px';
  window.CooldownHeader.style.top = gameUI.coolDownHeader.y * height + 'px';
  window.CooldownHeader.style.fontFamily = 'Menu';
  window.CooldownHeader.style.fontSize = gameUI.coolDownHeader.textSize + 'px';

  window.BombsHeader.innerText = 'Bombs';
  window.BombsHeader.style.left = gameUI.bombHeader.x * width + 'px';
  window.BombsHeader.style.top = gameUI.bombHeader.y * height + 'px';
  window.BombsHeader.style.fontFamily = 'Menu';
  window.BombsHeader.style.fontSize = gameUI.bombHeader.textSize + 'px';

  for (let i = 0; i < 10; i++) {
    let elem = document.getElementById('BombIcon' + i);
    elem.style.left =
      gameUI.bombIcons.x * width +
      i * gameUI.bombIcons.xInterval * width +
      'px';
    elem.style.top = gameUI.bombIcons.y * height + 'px';
  }

  HideUI();

  /*gameUI.gameGraphic.clear();
  gameUI.gameGraphic.textSize(gameUI.coolDownHeader.textSize);
  gameUI.gameGraphic.text('Weapon Cooldown',  * width, gameUI.coolDownHeader.y * height);
  gameUI.gameGraphic.textSize(gameUI.bombHeader.textSize);
  gameUI.gameGraphic.text('Bombs', gameUI.bombHeader.x * width, gameUI.bombHeader.y * height);
*/
}

function HideUI() {
  for (let i = 0; i < 10; i++) {
    let elem = document.getElementById('BombIcon' + i);
    elem.style.display = 'none';
  }

  window.BombsHeader.style.display = 'none';
  window.CooldownHeader.style.display = 'none';
  window.LevelHTML.style.display = 'none';
  window.ScoreHTML.style.display = 'none';
}

function drawUI() {
  noStroke();

  fill(gameColor.darkAlpha);
  rect(
    gameUI.fireBar.x * width,
    gameUI.fireBar.y * height,
    gameUI.fireBar.w * width,
    gameUI.fireBar.h * height,
  );

  fill(gameColor.lightAlpha);
  let barFillRatio = player.canFire
    ? player.fireDuration / player.FIRE_MAX_DURATION
    : ((player.fireDuration * 3) | 0) % 2;
  rect(
    gameUI.fireBar.x * width,
    gameUI.fireBar.y * height,
    barFillRatio * gameUI.fireBar.w * width,
    gameUI.fireBar.h * height,
  );

  if (
    !UI_DRAW_OPTIM.hasdrawGameUI ||
    UI_DRAW_OPTIM.lastBombsNb != player.bombsNb
  ) {
    window.CooldownHeader.style.display = 'inherit';
    window.BombsHeader.style.display = 'inherit';
    window.LevelHTML.style.display = 'inherit';
    window.ScoreHTML.style.display = 'inherit';

    for (let i = 0; i < 10; i++) {
      let elem = document.getElementById('BombIcon' + i);
      elem.style.display = i >= player.bombsNb ? 'none' : 'inherit';
    }

    UI_DRAW_OPTIM.lastBombsNb = player.bombsNb;
    UI_DRAW_OPTIM.hasdrawGameUI = true;
  }
}

function drawMenuUI() {
  push();
  gameUI.menuGraphic.clear();

  gameUI.menuGraphic.textSize(gameUI.menuTitle.textSize);
  gameUI.menuGraphic.text(
    'MUSHROOM MADNESS',
    gameUI.menuTitle.x * width,
    gameUI.menuTitle.y * height,
  );

  gameUI.menuGraphic.textSize(gameUI.menuPressPlay.textSize);
  gameUI.menuGraphic.text(
    'PRESS SPACE TO START',
    gameUI.menuPressPlay.x * width,
    gameUI.menuPressPlay.y * height,
  );

  gameUI.menuGraphic.textSize(gameUI.menuMoveKeysHeader.textSize);
  gameUI.menuGraphic.text(
    'MOVE',
    gameUI.menuMoveKeysHeader.x * width,
    gameUI.menuMoveKeysHeader.y * height,
  );

  gameUI.menuGraphic.image(
    IMGS.arrowsIcon,
    gameUI.menuArrowsIcon.x * width,
    gameUI.menuArrowsIcon.y * height,
  );

  gameUI.menuGraphic.textSize(gameUI.menuShootKeysHeader.textSize);
  gameUI.menuGraphic.text(
    'SHOOT',
    gameUI.menuShootKeysHeader.x * width,
    gameUI.menuShootKeysHeader.y * height,
  );

  gameUI.menuGraphic.image(
    IMGS.spaceBarIcon,
    gameUI.menuSpaceBarIcon.x * width,
    gameUI.menuSpaceBarIcon.y * height,
  );

  gameUI.menuGraphic.textSize(gameUI.menuBombKeysHeader.textSize);
  gameUI.menuGraphic.text(
    'BOMB',
    gameUI.menuBombKeysHeader.x * width,
    gameUI.menuBombKeysHeader.y * height,
  );

  gameUI.menuGraphic.image(
    IMGS.shiftIcon,
    gameUI.menuBombIcon.x * width,
    gameUI.menuBombIcon.y * height,
  );

  texture(gameUI.menuGraphic);
  plane(width, height);
  pop();
}

function drawPauseUI() {
  gameUI.gameGraphic.clear();

  gameUI.gameGraphic.textSize(gameUI.pauseHeader.textSize);
  gameUI.gameGraphic.text(
    'PAUSE',
    gameUI.pauseHeader.x * width,
    gameUI.pauseHeader.y * height,
  );
  texture(gameUI.gameGraphic);
  plane(width, height);
}

function drawGameOverMenu() {
  gameUI.gameGraphic.clear();

  gameUI.gameGraphic.textSize(gameUI.gameOverHeader.textSize);
  gameUI.gameGraphic.text(
    'GAME OVER!',
    gameUI.gameOverHeader.x * width,
    gameUI.gameOverHeader.y * height,
  );

  gameUI.gameGraphic.textSize(gameUI.gameOverScore.textSize);
  gameUI.gameGraphic.text(
    'SCORE: ' + GAME.score,
    gameUI.gameOverScore.x * width,
    gameUI.gameOverScore.y * height,
  );

  gameUI.gameGraphic.textSize(gameUI.gameOverLevel.textSize);
  gameUI.gameGraphic.text(
    'LEVEL: ' + GAME.lvl,
    gameUI.gameOverLevel.x * width,
    gameUI.gameOverLevel.y * height,
  );

  gameUI.gameGraphic.textSize(gameUI.gameOverPressPlay.textSize);
  gameUI.gameGraphic.text(
    'PRESS ESCAPE TO RESET',
    gameUI.gameOverPressPlay.x * width,
    gameUI.gameOverPressPlay.y * height,
  );

  texture(gameUI.gameGraphic);
  plane(width, height);
}
