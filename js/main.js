// @format
'use strict';

window.GAME = {
  lvl: 0,
  lvlTimer: 0,
  t: 0,
  deltaTime: 0,
  timeSpeed: 0,
  screenSize: 0,
  canvas: null,
  music: null,
  screenshakeStartTime: 0,
  screenshakeDuration: 0,
  screenshakeAmplitude: 0,
  screenshakeDir: 0,
  screenshakeRadius: 0,
  LVL_TRANSITION_DELAY: 1 / 4,
  LVL_TRANSITION_SLOW_FACTOR: 3,
  isOver: false,
  TIME_SPEED_INCREASE_FACTOR: 1 / 10,
  MAX_TIME_SPEED_FACTOR: 10,
  isInMenu: false,
  score: 0,
};

window.PAUSE = {
  isPaused: false,
  oldTimeSpeed: 1,
};

window.AUDIO = {};
window.FONTS = {};
window.IMGS = {};

function preload() {
  AUDIO.musics = [
    loadSound('audio/music2.mp3'),
    loadSound('audio/music4.mp3'),
    loadSound('audio/music6.mp3'),
  ];
  FONTS.menu = loadFont('fonts/MenuFont.ttf');

  IMGS.arrowsIcon = loadImage('imgs/arrowsIcons.png');
  IMGS.spaceBarIcon = loadImage('imgs/spaceIcon.png');
  IMGS.shiftIcon = loadImage('imgs/shiftIcon.png');
  IMGS.bombIcon = loadImage('imgs/bombIcon.png');

  //AUDIO.damageMush = loadSound('audio/damageMush.mp3');
  //AUDIO.deadPlayer = loadSound('audio/deadPlayer.mp3');
  //AUDIO.shoot = loadSound('audio/shoot.mp3');
  //AUDIO.levelComplet = loadSound('audio/levelComplet.mp3');
  //AUDIO.boom = loadSound('audio/boom.mp3');
}

function setup() {
  GAME.canvas = createCanvas(windowWidth, windowHeight, WEBGL);
  GAME.screenSize = min(width, height);

  frameRate(60);
  soundFormats('mp3');
  //AUDIO.damageMush.playMode('restart');
  //AUDIO.deadPlayer.playMode('restart');
  //AUDIO.shoot.playMode('restart');
  //AUDIO.levelComplet.playMode('restart');
  //AUDIO.boom.playMode('restart');

  setupTextGraphics();
  GAME.isInMenu = true;
  setupLvl(0); //lvl0 is menu
  //startNewMusic();
}

function lvlUp() {
  if (GAME.t > GAME.lvlTimer) {
    GAME.lvlTimer = GAME.t + GAME.LVL_TRANSITION_DELAY;
    GAME.timeSpeed *= GAME.LVL_TRANSITION_SLOW_FACTOR;

    player.checkCollisions = false;

    window.setTimeout(
      () => setupLvl(GAME.lvl + 1),
      GAME.LVL_TRANSITION_DELAY * GAME.LVL_TRANSITION_SLOW_FACTOR * 1e3,
    );
  }
}

function setupLvl(lvl) {
  GAME.lvl = lvl;
  GAME.timeSpeed = max(
    1000 / GAME.MAX_TIME_SPEED_FACTOR,
    1000 / (1 + lvl * GAME.TIME_SPEED_INCREASE_FACTOR),
  );

  setupMap(lvl);
  setupPlayer();
  initMushrooms(lvl);

  if (GAME.lvl > 1) {
    GAME.score += 100 + (lvl - 1) * 50;
  }
  //noCursor();
  startNewMusic();
}

function gameOver() {
  if (GAME.isInMenu) {
    return;
  }

  UI_DRAW_OPTIM.hasdrawGameUI = false;

  screenshake(0.2, 0.04);

  if (/debug/i.test(window.location.hash)) {
    return;
  }

  pauseGame();
  HideUI();
  GAME.isOver = true;
}

function resetGame() {
  if (PAUSE.isPaused) {
    pauseGame();
  }

  resetPlayer();
  GAME.isInMenu = true;
  GAME.isOver = false;
  GAME.score = 0;
  setupLvl(0);

  startNewMusic();
}

function startNewMusic() {
  if (GAME.music) GAME.music.stop();
  GAME.music = random(AUDIO.musics);
  GAME.music.setLoop(true);
  GAME.music.play();
}

function keyPressed() {
  if (GAME.isOver && (keyCode === ESCAPE || keyCode === 32)) resetGame();

  if (GAME.lvl !== 0 && !GAME.isOver && keyCode === ESCAPE) pauseGame();
}

function pauseGame() {
  if (PAUSE.isPaused) {
    GAME.timeSpeed = PAUSE.oldTimeSpeed;
  } else {
    PAUSE.oldTimeSpeed = GAME.timeSpeed;
    GAME.timeSpeed = 10000;
  }

  PAUSE.isPaused = !PAUSE.isPaused;
}

function update() {
  GAME.deltaTime =
    (window.performance.now() - GAME.canvas._pInst._lastFrameTime) /
    GAME.timeSpeed;

  GAME.t += GAME.deltaTime;

  updateBullets();
  updateBombs();

  if (GAME.isInMenu) {
    updateMenu();
  } else if (!GAME.isOver) {
    updatePlayer();
  }

  updateColors();
  updateMushrooms();
  updateScreenshake();
}

function draw() {
  update();

  background(gameColor.bright);

  push();
  drawScreenshake();

  push();
  rotatePlayerView();
  drawMap();
  drawMushrooms();
  drawBullets();
  drawBombs();
  pop();

  drawPlayer();
  pop();

  if (GAME.isOver) {
    drawGameOverMenu();
  } else if (PAUSE.isPaused) {
    drawPauseUI();
  } else if (GAME.isInMenu) {
    drawMenuUI();
  } else {
    drawUI();
  }
}

function rotatePlayerView() {
  translate(0, player.SCREEN_Y * GAME.screenSize, 0);

  let lvlTransRotate =
    GAME.lvlTimer > GAME.t
      ? ((GAME.t - GAME.lvlTimer + GAME.LVL_TRANSITION_DELAY) /
          GAME.LVL_TRANSITION_DELAY) *
        TWO_PI
      : 0;

  rotateZ(PI * 1.5 + player.dir - lvlTransRotate);
}

function screenshake(amplitude, duration) {
  GAME.screenshakeStartTime = GAME.t;
  GAME.screenshakeDuration = duration;
  GAME.screenshakeAmplitude = amplitude;
}

function updateScreenshake() {
  if (GAME.t < GAME.screenshakeStartTime + GAME.screenshakeDuration) {
    let halfDuration = GAME.screenshakeDuration / 2;
    let k =
      1 -
      abs(halfDuration - (GAME.t - GAME.screenshakeStartTime)) / halfDuration;
    GAME.screenshakeDir = random(TWO_PI);
    GAME.screenshakeRadius = random(lerp(0, GAME.screenshakeAmplitude, k * k));
  }
}

function drawScreenshake() {
  if (GAME.t < GAME.screenshakeStartTime + GAME.screenshakeDuration) {
    translate(
      cos(GAME.screenshakeDir) * GAME.screenshakeRadius * GAME.screenSize,
      sin(GAME.screenshakeDir) * GAME.screenshakeRadius * GAME.screenSize,
      0,
    );
  }
}
